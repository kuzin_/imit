import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class FinanceReport {
    private Payment[] array;
    private String payer;
    private int dayReport;
    private int monthReport;
    private int yearReport;

    public FinanceReport(@NotNull String payer, @NotNull int dayReport, @NotNull int monthReport, @NotNull int yearReport){
        if ((dayReport > 32) || (dayReport < 1)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((dayReport == 31) && ((monthReport == 2) || (monthReport == 4) ||
                (monthReport == 6) || (monthReport == 9) || (monthReport == 11))){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((dayReport == 29) && (yearReport % 4 != 0)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((monthReport > 12) || (monthReport < 1)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        this.payer = payer;
        this.dayReport = dayReport;
        this.monthReport = monthReport;
        this.yearReport = yearReport;
    }

    public FinanceReport(FinanceReport r) {
        this(r.getPayer(), r.getDayReport(), r.getMonthReport(), r.getYearReport(), r.getArray());
    }

    public FinanceReport(@NotNull String payer, @NotNull int dayReport, @NotNull int monthReport, @NotNull int yearReport, Payment[] payments){
        if ((dayReport > 32) || (dayReport < 0)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((dayReport == 31) && ((monthReport == 2) || (monthReport == 4) ||
                (monthReport == 6) || (monthReport == 9) || (monthReport == 11))){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((dayReport == 29) && (yearReport % 4 != 0)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        if ((monthReport > 12) || (monthReport < 1)){
            throw new IllegalArgumentException("Wrong date in report");
        }
        this.payer = payer;
        this.dayReport = dayReport;
        this.monthReport = monthReport;
        this.yearReport = yearReport;
        this.array = new Payment[payments.length];
        int i = 0;
        for (Payment p : payments){
            this.array[i] = new Payment(p);
            i++;
        }
    }

    public String getPayer() {
        return payer;
    }

    public int getDayReport() {
        return dayReport;
    }

    public int getMonthReport() {
        return monthReport;
    }

    public int getYearReport() {
        return yearReport;
    }

    public int getPaymentsNumber(){
        return array.length;
    }

    public Payment getPayment(int idx){
        return array[idx];
    }

    public void setPaymentByPayment(int idx, Payment p){
        array[idx].setPersonFullName(p.getPersonFullName());
        array[idx].setDayOfPayment(p.getDayOfPayment());
        array[idx].setMonthOfPayment(p.getMonthOfPayment());
        array[idx].setYearOfPayment(p.getYearOfPayment());
        array[idx].setAmountOfPayment(p.getAmountOfPayment());
    }

    public void setPayment(int idx, String name, int day, int month, int year, int paymentAmount){
        array[idx].setPersonFullName(name);
        array[idx].setDayOfPayment(day);
        array[idx].setMonthOfPayment(month);
        array[idx].setYearOfPayment(year);
        array[idx].setAmountOfPayment(paymentAmount);
    }

    public Payment[] getArray() {
        return array;
    }

    @Override
    public String toString() {
        String text = "";
        String name = String.format("[Автор: %s, дата: %d.%d.%d, платежи:[ \n", getPayer(), getDayReport(),
                getMonthReport(), getYearReport());
        for (Payment toString : array) {
            text = new StringBuilder().append(text).append(String.format("[%s: %s, дата: %d.%d.%d сумма: %d руб. %d коп.] \n",
                    payer, toString.getPersonFullName(), toString.getDayOfPayment(),
                    toString.getMonthOfPayment(), toString.getYearOfPayment(),
                    toString.getAmountOfPayment() / 100, toString.getAmountOfPayment() % 100)).toString();
        }
        text.toString();
        name = String.join("", name, text);
        return name;
    }

    public Payment[] paymentsCopy(){
        Payment[] copy = new Payment[array.length];
        for (int i = 0; i < array.length; i++){
            copy[i] = new Payment(this.array[i]);
        }
        return copy;
    }

}
