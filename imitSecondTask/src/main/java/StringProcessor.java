
import org.jetbrains.annotations.NotNull;

public class StringProcessor {

    //Строка из n копий строки s
    public String stringOfNumberCopies(@NotNull String s, int n){
        String resultString = "";
        StringBuilder result = new StringBuilder();
        if (n==0){
            return resultString;
        }
        if (n < 0){
            throw new IllegalArgumentException("N < 0");
        }
        for (int i = 0; i < n; i++){
            result.append(s);
        }
        return result.toString();
    }

    // Количество вхождений строки(substring) в строку(string)
    public int numberOfOccurrences(String string, String substring){
        if (substring == null || substring.isEmpty()){
            throw new IllegalArgumentException("String is empty");
        }
        if ((string.isEmpty()) || (string.equals(""))){
            throw new IllegalArgumentException("String is empty");
        }
        return (string.length() - string.replace(substring, "").length()) / substring.length();
    }

    // Построение строки из исходной(s) с заменой
    public String newStringWithReplace(@NotNull String s){
        return s.
                replace("1", "один").
                replace("2","два").
                replace("3","три");
    }

    // Удаление из строки s каждого второго символа
    public StringBuilder deleteEvenSymbol(StringBuilder s){
        for (int i = 1; i < s.length(); i++){
            s.deleteCharAt(i);
        }
        return s;
    }

    // Реверс строки, состоящей из слов, в обратном порядке // Закончить позже
    public String reverseWordsInLine(@NotNull String s){
        String[] words = s.split(" ");
        return s;
    }

    // Преобразование подстроки вида 0хNNNNNNNN
    public String hexAgeToIntProvider(@NotNull String s){
        String find = "0x";
        while (s.contains(find)) {
            char[] arrayOfChars = new char[8];
            int a = s.indexOf(find);
            String exchangeS = s.substring(a + 2, a + 10);
            int degree = 7;
            int sumYear = 0;
            for (char year : exchangeS.toCharArray()) {
                sumYear += Math.pow(16, degree) * Character.digit(year, 16);
                degree--;
            }
            degree = 7;
            String yearInString = Integer.toString(sumYear);
            s = s.replaceAll(exchangeS, yearInString).replaceFirst("0x", "");
        }
        return s;
    }
}
