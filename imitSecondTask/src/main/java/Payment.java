import java.util.Objects;

public class Payment {

    private String personFullName;
    private int dayOfPayment;
    private int monthOfPayment;
    private int yearOfPayment;
    private int amountOfPayment;

    public Payment(String personFullName, int dayOfPayment, int monthOfPayment, int yearOfPayment, int amountOfPayment) {
        this.personFullName = personFullName;
        this.dayOfPayment = dayOfPayment;
        this.monthOfPayment = monthOfPayment;
        this.yearOfPayment = yearOfPayment;
        this.amountOfPayment = amountOfPayment;
    }

    public Payment(){

    }

    public Payment(Payment payment) {
        this.personFullName = payment.getPersonFullName();
        this.dayOfPayment = payment.getDayOfPayment();
        this.monthOfPayment = payment.getMonthOfPayment();
        this.yearOfPayment = payment.getYearOfPayment();
        this.amountOfPayment = payment.getAmountOfPayment();
    }

    @Override
    public String toString() {
        return "Payment{" +
                "personFullName='" + personFullName + '\'' +
                ", dayOfPayment=" + dayOfPayment +
                ", monthOfPayment=" + monthOfPayment +
                ", yearOfPayment=" + yearOfPayment +
                ", amountOfPayment=" + amountOfPayment +
                '}';
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public int getDayOfPayment() {
        return dayOfPayment;
    }

    public int getMonthOfPayment() {
        return monthOfPayment;
    }

    public int getYearOfPayment() {
        return yearOfPayment;
    }

    public int getAmountOfPayment() {
        return amountOfPayment;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }

    public void setDayOfPayment(int dayOfPayment) {
        this.dayOfPayment = dayOfPayment;
    }

    public void setMonthOfPayment(int monthOfPayment) {
        this.monthOfPayment = monthOfPayment;
    }

    public void setYearOfPayment(int yearOfPayment) {
        this.yearOfPayment = yearOfPayment;
    }

    public void setAmountOfPayment(int amountOfPayment) {
        this.amountOfPayment = amountOfPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return dayOfPayment == payment.dayOfPayment &&
                monthOfPayment == payment.monthOfPayment &&
                yearOfPayment == payment.yearOfPayment &&
                amountOfPayment == payment.amountOfPayment &&
                personFullName.equals(payment.personFullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personFullName, dayOfPayment, monthOfPayment, yearOfPayment, amountOfPayment);
    }
}
