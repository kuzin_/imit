public class FinanceReportProcessor {


    public static FinanceReport getPaymentsWithChar(String c, FinanceReport report){
        int count = 0;
        for (int i = 0; i < report.getPaymentsNumber(); i++){
            if (report.getPayment(i).getPersonFullName().startsWith(c)){
                count++;
            }
        }
        Payment[] array = new Payment[count];
        count = 0;
        for (int i = 0; i < report.getPaymentsNumber(); i++){
            if (report.getPayment(i).getPersonFullName().startsWith(c)){
                array[count] = report.getPayment(i);
                count++;
            }
        }
        FinanceReport result = new FinanceReport(report.getPayer(), report.getDayReport(), report.getMonthReport(),
                report.getYearReport(), array);
        return result;
    }

    public static FinanceReport getPaymentsWithSomeAmount(int amount, FinanceReport report){
        int count = 0;
        for (int i = 0; i < report.getPaymentsNumber(); i++){
            if (report.getPayment(i).getAmountOfPayment() < amount){
                count++;
            }
        }
        Payment[] array = new Payment[count];
        count = 0;
        for (int i = 0; i < report.getPaymentsNumber(); i++){
            if (report.getPayment(i).getAmountOfPayment() < amount){
                array[count] = report.getPayment(i);
                count++;
            }
        }
        FinanceReport result = new FinanceReport(report.getPayer(), report.getDayReport(), report.getMonthReport(),
                report.getYearReport(), array);
        return result;
    }

    // Доп. задание

    public int sumPayForDate(String dateInString, FinanceReport report){
        int sumOfAmount = 0;
        String[] dates = dateInString.split(".");
        int[] date = new int[3];
        int i = 0;
        for (String string : dates){
            date[i] = Integer.parseInt(string);
            i++;
        }
        for (i = 0; i < report.getPaymentsNumber(); i++){
            if ((report.getPayment(i).getDayOfPayment() == date[0]) &&
                    (report.getPayment(i).getMonthOfPayment() == date[1]) &&
                    (report.getPayment(i).getYearOfPayment() == date[2])){
                sumOfAmount += report.getPayment(i).getAmountOfPayment();
            }
        }
        return sumOfAmount;
    }



}
