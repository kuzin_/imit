import org.junit.Assert;
import org.junit.Test;

public class TestForFinanceReportProcessor {

    FinanceReportProcessor forTest = new FinanceReportProcessor();

    @Test
    public void testGetPaymentWithChar(){
        Payment a  = new Payment("Andrey", 4, 12, 2020, 0);
        Payment b  = new Payment("Ivan", 5, 12, 2020, 0);
        Payment c  = new Payment("Aleksey", 4, 12, 2020, 1);
        Payment[] array = new Payment[] {a, b, c};
        FinanceReport test = new FinanceReport("Tester", 4, 12, 2020, array);
        FinanceReport test2 = forTest.getPaymentsWithChar("A", test);
        Assert.assertEquals("[Tester: Andrey, дата: 4.12.2020 сумма: 0 руб. 0 коп.] \n" +
                "[Tester: Aleksey, дата: 4.12.2020 сумма: 0 руб. 1 коп.] \n" ,test2.toString());
    }

    @Test
    public void testGetPaymentWithAmount(){
        Payment a  = new Payment("Tester", 4, 12, 2020, 0);
        Payment b  = new Payment("Tester", 5, 12, 2020, 1);
        Payment c  = new Payment("Tester", 6, 12, 2020, 0);
        Payment[] array = new Payment[] {a, b, c};
        FinanceReport test = new FinanceReport("Tester", 4, 12, 2020, array);
        FinanceReport test2 = forTest.getPaymentsWithSomeAmount(1, test);
        Assert.assertEquals("[Tester: Tester, дата: 4.12.2020 сумма: 0 руб. 0 коп.] \n" +
                "[Tester: Tester, дата: 6.12.2020 сумма: 0 руб. 0 коп.] \n" ,test2.toString());
    }



}
