
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestForStringProcessor {
    StringProcessor testProcessor = new StringProcessor();


    @Test
    public void stringNumberCopiesTest(){
        String string = "NO";
        int n = 3;
        String expected = "";
        for (int i = 0; i < n; i++){
            expected += string;
        }
        String actual = testProcessor.stringOfNumberCopies(string, n);
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void occurrenceTest(){
        String string = "TestTestTest";
        int actual = testProcessor.numberOfOccurrences(string, "Test");
        Assert.assertEquals(3, actual);
    }

    @Test
    public void replaceTest(){
        String expected = "одиндватри";
        String beforeReplace = "123";
        String actual = testProcessor.newStringWithReplace(beforeReplace);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteEvenCharTest(){
        StringBuilder s = new StringBuilder("123456789");
        StringBuilder sb = testProcessor.deleteEvenSymbol(s);
        String expected = "13579";
        Assert.assertEquals(sb.toString(), expected);
    }

    @Test
    public void testHexAgeToInt(){
        String s = "Васе 0x00000010 лет, Андрею 0x00000011 лет";
        Assert.assertEquals(testProcessor.hexAgeToIntProvider(s), "Васе 16 лет, Андрею 17 лет");
    }
}
