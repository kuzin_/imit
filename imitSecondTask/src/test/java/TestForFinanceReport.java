import org.junit.Assert;
import org.junit.Test;

public class TestForFinanceReport {
    //FinanceReport beforeTest = new FinanceReport("Tester", 4, 12, 2020);

    @Test
    public void testGetPayment(){
        Payment a  = new Payment("Tester", 4, 11, 2020, 0);
        Payment b  = new Payment("Tester", 5, 11, 2020, 0);
        Payment c  = new Payment("Tester", 6, 11, 2020, 1000);
        Payment[] array = new Payment[] {a, b, c};
        FinanceReport test = new FinanceReport("Tester", 6, 11, 2020, array);
        String expectedName = "Tester";
        int expectedDay = 6;
        int expectedMonth = 11;
        int expectedYear = 2020;
        int expectedAmount = 1000;
        Assert.assertEquals(test.getPayment(2).getPersonFullName(), expectedName);
        Assert.assertEquals(test.getPayment(2).getDayOfPayment(), expectedDay);
        Assert.assertEquals(test.getPayment(2).getMonthOfPayment(), expectedMonth);
        Assert.assertEquals(test.getPayment(2).getYearOfPayment(), expectedYear);
        Assert.assertEquals(test.getPayment(2).getAmountOfPayment(), expectedAmount);
    }

    @Test
    public void copyTest(){
        Payment a  = new Payment("Tester", 4, 12, 2020, 0);
        Payment b  = new Payment("Tester", 4, 12, 2020, 0);
        Payment c  = new Payment("Tester", 4, 12, 2020, 0);
        Payment[] array = new Payment[] {a, b, c};
        FinanceReport test = new FinanceReport("Tester", 4, 12, 2020, array);
        FinanceReport test2 = new FinanceReport(test);



        test.getPayment(0).setAmountOfPayment(100);
        int amount1 = test.getPayment(0).getAmountOfPayment();
        int amount2 = test2.getPayment(0).getAmountOfPayment();
        Assert.assertNotEquals(amount1, amount2);
        int expected = test.getPayment(1).getAmountOfPayment();
        int actual = test2.getPayment(1).getAmountOfPayment();
        Assert.assertEquals(expected, actual);
    }

}
