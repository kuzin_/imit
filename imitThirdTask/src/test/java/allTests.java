
import org.junit.Assert;
import org.junit.Test;

public class allTests {

    @Test
    public void filterBeginTest(){
        String s = "Hello, World";
        beginStringFilter filter1 = new beginStringFilter("Hello");
        boolean actual = filter1.apply(s);
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void filterEndTest(){
        String s = "Hello, World";
        EndStringFilter filter1 = new EndStringFilter("World");
        boolean actual = filter1.apply(s);
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void serviceTest(){
        productService service = new productService();
        String s = "Fruits";
        beginStringFilter filter1 = new beginStringFilter("A");
        EndStringFilter filter2 = new EndStringFilter("O");
        WordLengthFilter filter3 = new WordLengthFilter("5");
        productPackage pack = new productPackage("Fruit Package", 1);
        weightProduct wProduct = new weightProduct("Apple", "This is Fruit");
        packedWeightProduct p1 = new packedWeightProduct(wProduct, 1, pack);
        pieceProduct pProduct = new pieceProduct("Orange", "This is Fruit", 1);
        packedPieceProduct p2 = new packedPieceProduct(pProduct, pack, 2);
        product[] array = new product[] {p1, p2};
        consignmentsOfProducts set = new consignmentsOfProducts(array, "Smth");
        int expected = 1;
        int actual = service.countByFilter(set, filter1);
        Assert.assertEquals(expected, actual);
        Assert.assertEquals(5, set.getGrossWeight(), 1e-9);
    }
}
