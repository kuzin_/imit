
import java.util.Objects;

public class packedWeightProduct extends weightProduct implements IPacked {

    private double productWeight;
    private double packageWeight;
    private String packedProduct;

    public packedWeightProduct(weightProduct product, double weight, productPackage pack) {
        super(product.getName(), product.getDescription());
        if (weight < 0){
            throw new IllegalArgumentException("Weight can't be < 0");
        }
        this.productWeight = weight;
        this.packageWeight = pack.getPackageWeight();
    }

    public double getNetWeight(){
        return productWeight;
    }

    public double getPackageWeight() {
        return packageWeight;
    }

    public String getPackedProductName() {
        return packedProduct;
    }

    @Override
    public double getGrossWeight() {
        return getNetWeight() + getPackageWeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        packedWeightProduct that = (packedWeightProduct) o;
        return Double.compare(that.productWeight, productWeight) == 0 &&
                Double.compare(that.packageWeight, packageWeight) == 0 &&
                Objects.equals(packedProduct, that.packedProduct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), productWeight, packageWeight, packedProduct);
    }
}
