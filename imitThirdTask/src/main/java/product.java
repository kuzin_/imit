import java.util.Objects;

public abstract class product implements IPacked {
    private String name;
    private String description;

    public product(String name, String description) {
        if (name.isEmpty()){
            throw new IllegalArgumentException("Name is empty");
        }
        if (description.isEmpty()){
            throw new IllegalArgumentException("Description is empty");
        }
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        product product = (product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
