import java.util.Objects;

public class productPackage {

    private String packageName;
    private double packageWeight;

    public productPackage(String name, double weight){
        if (weight < 0){
            throw new IllegalArgumentException("Weight can't be < 0");
        }
        this.packageName = name;
        this.packageWeight = weight;
    }

    public String getPackageName() {
        return packageName;
    }

    public double getPackageWeight() {
        return packageWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        productPackage that = (productPackage) o;
        return Double.compare(that.packageWeight, packageWeight) == 0 &&
                Objects.equals(packageName, that.packageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageName, packageWeight);
    }

    @Override
    public String toString() {
        return "productPackage{" +
                "packageName='" + packageName + '\'' +
                ", packageWeight=" + packageWeight +
                '}';
    }
}
