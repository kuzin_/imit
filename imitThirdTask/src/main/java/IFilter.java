
public interface IFilter {
    public boolean apply(String s) throws IllegalArgumentException;
}
