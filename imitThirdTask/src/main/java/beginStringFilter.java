
public class beginStringFilter implements IFilter {

    private String pattern;

    public beginStringFilter(String s){
        this.pattern = s;
    }
    @Override
    public boolean apply(String s) throws IllegalArgumentException {
        if (s.isEmpty()){
            throw new IllegalArgumentException("String is empty");
        }
        return s.startsWith(pattern);
    }
}
