import java.util.Objects;

public class pieceProduct extends product implements IPacked {
    private double weight;

    public pieceProduct(String name, String description, double weight) {
        super(name, description);
        this.weight = weight;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        pieceProduct that = (pieceProduct) o;
        return Double.compare(that.weight, weight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight);
    }

    @Override
    public String toString() {
        return "pieceProduct{" +
                "weight=" + weight +
                "} " + super.toString();
    }

    @Override
    public double getGrossWeight() {
        return getWeight();
    }
}
