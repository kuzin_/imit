public class EndStringFilter implements IFilter {

    private String pattern;

    public EndStringFilter(String s){
        this.pattern = s;
    }

    @Override
    public boolean apply(String s) throws IllegalArgumentException {
        if (s.isEmpty()){
            throw new IllegalArgumentException("String is empty");
        }
        return s.endsWith(pattern);
    }
}
