
import java.util.Arrays;
import java.util.Objects;

public class consignmentsOfProducts implements IPacked {

    private product[] array;
    private String description;
    public consignmentsOfProducts(product[] p, String s){
        this.array = new product[p.length];
        System.arraycopy(p,0,this.array,0,p.length);
        this.description = s;
    }

    public int productCount(){
        return array.length;
    }

    public product[] getArray() {
        return array;
    }

    public product getElem(int idx){
        return array[idx];
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        consignmentsOfProducts that = (consignmentsOfProducts) o;
        return Arrays.equals(array, that.array) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(description);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }

    @Override
    public double getGrossWeight() {
        double result = 0;
        for (IPacked item : array){
            result += item.getGrossWeight();
        }
        return result;
    }


}
