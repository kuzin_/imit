public class WordLengthFilter implements IFilter{

    private String pattern;

    public WordLengthFilter(String s){
        this.pattern = s;
    }

    @Override
    public boolean apply(String s) throws IllegalArgumentException {
        if (s.isEmpty()){
            throw new IllegalArgumentException("String is empty");
        }
        return s.length() == pattern.length();
    }
}
