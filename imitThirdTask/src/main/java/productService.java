
public class productService {
    public int countByFilter(consignmentsOfProducts p, IFilter f){
        int result = 0;
        for (int i = 0; i < p.productCount(); i++){
            if(f.apply(p.getElem(i).getName())){
                result++;
            }
        }
        return result;
    }
}
