import java.util.Objects;

public class packedPieceProduct extends pieceProduct implements IPacked {

    private int pieceAmount;
    private double packageWeight;
    private double pieceWeight;
    private String packName;

    public packedPieceProduct(pieceProduct product, productPackage pack, int pieceAmount) {
        super(product.getName(),product.getDescription(), product.getWeight());
        this.pieceAmount = pieceAmount;
        this.packageWeight = pack.getPackageWeight();
        this.packName = pack.getPackageName();
    }

    public int getProductsAmount(){
        return pieceAmount;
    }

    public double getPackageWeight() {
        return packageWeight;
    }

    public String getPackedProductName() {
        return packName;
    }

    @Override
    public double getWeight() {
        return super.getWeight();
    }

    public double getPieceWeight() {
        return pieceWeight;
    }

    public double getNetWeight(){
        return getProductsAmount()*getWeight();
    }

    @Override
    public double getGrossWeight() {
        return getNetWeight() + getPackageWeight();
    }

    @Override
    public String toString() {
        return "packedPieceProduct{" +
                "pieceAmount=" + pieceAmount +
                ", packageWeight=" + packageWeight +
                ", pieceWeight=" + pieceWeight +
                ", packName='" + packName + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        packedPieceProduct that = (packedPieceProduct) o;
        return pieceAmount == that.pieceAmount &&
                Double.compare(that.packageWeight, packageWeight) == 0 &&
                Double.compare(that.pieceWeight, pieceWeight) == 0 &&
                Objects.equals(packName, that.packName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pieceAmount, packageWeight, pieceWeight, packName);
    }
}