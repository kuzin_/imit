package com.company;

public class Vector3D {
    private double x;
    private double y;
    private double z;

    public double getVectorX(){
        return this.x;
    }

    public double getVectorY(){
        return this.y;
    }

    public double getVectorZ(){
        return this.z;
    }

    public void setVectorX(double x){
        this.x = x;
    }

    public void setVectorY(double y){
        this.y = y;
    }

    public void setVectorZ(double z){
        this.z = z;
    }

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Vector3D(Point3D startPoint3D, Point3D finalPoint3D) {
        this.x = finalPoint3D.getX() - startPoint3D.getX();
        this.y = finalPoint3D.getY() - startPoint3D.getY();
        this.z = finalPoint3D.getZ() - startPoint3D.getZ();
    }

    @Override
    public String toString() {
        return "Vector3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }


    public double vectorLength() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public boolean vectorEquals(Vector3D vector){
        return (((this.x) == (vector.x)) && ((this.y) == (vector.y)) && ((this.z) == (vector.z)));
    }

}

