package com.company;

public class Vector3DArray {

    private Vector3D[] array;

    public Vector3DArray(int number) {
        if (number < 1) {
            throw new IllegalArgumentException("Array length < 1");
        }
        array = new Vector3D[number];
        for (int i = 0; i < number; i++) {
            array[i] = new Vector3D();
        }
    }

    public int arrayLength() {
        return array.length;
    }

    public void exchangeVectorInArray(int numberForExchange, Vector3D vector) {
        if (numberForExchange < 0) {
            throw new IllegalArgumentException("number < 0");
        }
        Vector3D exchangeVector = new Vector3D(vector.getVectorX(), vector.getVectorY(), vector.getVectorZ());
        this.array[numberForExchange] = exchangeVector;
    }

    public double maxArrayVectorLength() {
        double maxLength = 0;
        for (int i = 0; i < arrayLength(); i++) {
            if (array[i].vectorLength() > maxLength) {
                maxLength = array[i].vectorLength();
            }
        }
        return maxLength;
    }

    public int findGivenVector(Vector3D vector) {
        int index = -1;
        for (int i = 0; i < arrayLength(); i++) {
            if (array[i].vectorEquals(vector)) {
                index = i;
                return index;
            }
        }
        return index;
    }

    public Vector3D sumArrayVectors() {
        Vector3DProcessor vectorForCalculate = new Vector3DProcessor();
        Vector3D resultVector = new Vector3D();
        for (int i = 0; i < arrayLength(); i++) {
            resultVector = vectorForCalculate.vectorsSum(array[i], resultVector);
        }
        return resultVector;
    }

    public Vector3D linearVectorSum(double[] coef) {
        Vector3D resultVector = new Vector3D();
        Vector3DProcessor vectorForCalculate = new Vector3DProcessor();
        if (array == null) {
            throw new IllegalArgumentException("Array is empty");
        }
        if (arrayLength() != coef.length) {
            throw new IllegalArgumentException("Vector array length != Coefficient array length");
        }
        for (int i = 0; i < arrayLength(); i++) {
            resultVector = vectorForCalculate.vectorsSum(resultVector, vectorForCalculate.multVectorByNumber(coef[i], array[i]));
        }
        return resultVector;
    }

    public Point3D[] shiftedPoints(Point3D point) {
        Point3D[] arr = new Point3D[arrayLength()];
        for (int i = 0; i < arrayLength(); i++) {
            arr[i] = new Point3D(point.getX() + array[i].getVectorX(), point.getY() + array[i].getVectorY(), point.getZ() + array[i].getVectorZ());
        }
        return arr;
    }

    public void showVectorsArray() {
        for (int i = 0; i < arrayLength(); i++) {
            String string = array[i].toString();
            System.out.println(string);
        }
    }

}
