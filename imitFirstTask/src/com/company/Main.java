package com.company;

public class Main {
    public static void main(String[] args) {
        Vector3DArray vArray = new Vector3DArray(4);
        Vector3DProcessor vProcessor = new Vector3DProcessor();

        System.out.println("Проверка Vector3DProcessor");
        Vector3D vector1 = new Vector3D(1, 2, 1);
        Vector3D vector2 = new Vector3D(2, 1, 2);
        Vector3D sumVector = vProcessor.vectorsSum(vector1, vector2);
        Vector3D subVector = vProcessor.vectorsSubtraction(vector1,vector2);
        String string1 = sumVector.toString();
        String string2 = subVector.toString();
        System.out.println("Сумма векторов " + string1);
        System.out.println("Разница векторов " + string2);
        System.out.println("Скалярное произведение: " + vProcessor.scalarProduct(vector1, vector2));
        System.out.println("Векторное произведение: " + vProcessor.vectorProduct(vector1, vector2));
        System.out.println("Коллинеарность векторов: " + vProcessor.isCollinearVectors(vector1, vector2));
        System.out.println();
        System.out.println();


        System.out.println("Проверка Vector3DArray");
        //vArray.arrayOfVectors3D(4);
        System.out.println("Длина массива: " + vArray.arrayLength());
        vArray.exchangeVectorInArray(2, vector1);
        vArray.showVectorsArray();
        double result = vArray.maxArrayVectorLength();
        System.out.println("Максимальная длина вектора в массиве: " + result);
        int n = vArray.findGivenVector(vector1);
        System.out.println("Индекс входящего вектора: " + n);
        Vector3D sumVector2 = vArray.sumArrayVectors();
        String string = sumVector2.toString();
        System.out.println("Сумма векторов в массиве: " + string);
        double[] coefArray = new double[] {1, 2, 3, 5};
        Vector3D resultVector = vArray.linearVectorSum(coefArray);
        String resultVectorString = resultVector.toString();
        System.out.println(resultVectorString);
        vArray.showVectorsArray();
        Point3D point = new Point3D(10, 0, 11);
        Point3D[] resultPoint = vArray.shiftedPoints(point);
        for (int i = 0; i < vArray.arrayLength(); i++){
            System.out.println("Точка номер " + (i));
            resultPoint[i].info();
        }

    }
}

