package com.company;

public class Vector3DProcessor {

    public Vector3D vectorsSum(Vector3D first, Vector3D second){
        double x = first.getVectorX() + second.getVectorX();
        double y = first.getVectorY() + second.getVectorY();
        double z = first.getVectorZ() + second.getVectorZ();
        return new Vector3D(x, y, z);
    }

    public Vector3D vectorsSubtraction(Vector3D first, Vector3D second){
        double x = second.getVectorX() - first.getVectorX();
        double y = second.getVectorY() - first.getVectorY();
        double z = second.getVectorZ() - first.getVectorZ();
        return new Vector3D(x, y, z);
    }

    public double scalarProduct(Vector3D first, Vector3D second){
        return first.getVectorX() * second.getVectorX() + first.getVectorY() + second.getVectorY() + first.getVectorZ() * second.getVectorZ();
    }


    public Vector3D vectorProduct(Vector3D first, Vector3D second){
        double x = first.getVectorY()* second.getVectorZ() - first.getVectorY() * second.getVectorY();
        double y = first.getVectorZ() * second.getVectorX() - first.getVectorX() * second.getVectorZ();
        double z = first.getVectorZ() * second.getVectorY() - first.getVectorY() * second.getVectorZ();
        return new Vector3D(x,y,z);
    }


    public boolean isCollinearVectors(Vector3D first,Vector3D second){
        if (Math.abs(first.getVectorY() * second.getVectorZ() - second.getVectorY() * first.getVectorZ()) < 1e-13 &&
                Math.abs(first.getVectorX() * second.getVectorZ() - second.getVectorX() * first.getVectorX()) < 1e-13
                && Math.abs(first.getVectorX() * second.getVectorY() - second.getVectorX() * first.getVectorY()) < 1e-13) {
            return true;
        }
        return false;
    }

    public Vector3D multVectorByNumber(double number, Vector3D vector){
        double x = number * vector.getVectorX();
        double y = number * vector.getVectorY();
        double z = number * vector.getVectorZ();
        Vector3D resultVector = new Vector3D(x, y, z);
        return resultVector;
    }


}

