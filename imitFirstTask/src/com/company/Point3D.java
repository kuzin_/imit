package com.company;

import java.util.Objects;

public class Point3D {
    private double x;
    private double y;
    private double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void info(){
        System.out.println("Значение по оси Х: " + getX());
        System.out.println("Значение по оси Y: " + getY());
        System.out.println("Значение по оси Z: " + getZ());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public static void main(String[] args){
        Point3D firstPoint3D = new Point3D(10,0,-10);
        Point3D secondPoint3D = new Point3D();
        System.out.println(firstPoint3D.equals(secondPoint3D));
        System.out.println(firstPoint3D.equals(firstPoint3D));
        if(firstPoint3D == secondPoint3D) {
            System.out.println("==");
        }
    }


}
